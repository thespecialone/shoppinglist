//
//  NewItemViewController.m
//  ShoppingList_0.1
//
//  Created by kravinov on 12/17/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "NewItemViewController.h"
#import <CoreData/CoreData.h>

@interface NewItemViewController ()

@end

@implementation NewItemViewController

#pragma mark - View Controller Lifecycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Save New Item Method

- (IBAction)saveItemButton:(id)sender {
    if (self.nameTextField.text.length != 0) {
        NSManagedObjectContext *context = [self managedObjectContext];
        
        NSManagedObject *newItem = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem setValue:self.nameTextField.text forKey:@"itemName"];
        
        if (self.categoryTextField.text.length != 0) {
            [newItem setValue:self.categoryTextField.text forKey:@"itemCategory"];
        } else {
            [newItem setValue:@"Другое" forKey:@"itemCategory"];
        }
        
        NSError *error = nil;
        if(![context save:&error]){
            NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
        }
        else {
            NSLog(@"Saved successfully!");
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        UIAlertView *errAlert = [[UIAlertView alloc] initWithTitle:@"Недопустимое наименование товара"
                                                             message:@"Не нужно сохранять объект с пустым наименованием, заполни поле, пожалуйста"
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"OK", nil];
        [errAlert setAlertViewStyle:UIAlertViewStyleDefault];
        [errAlert show];
        NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentPath = [searchPaths objectAtIndex:0];
        NSLog(@"%@", documentPath);
    }
    

}

#pragma mark - Select Category Method

- (IBAction)selectCategoryButton:(id)sender {
    UIButton *categoryButton = (UIButton*)sender;
    
    switch (categoryButton.tag) {
        case 0: {
            NSLog(@"Выпечка");
            self.categoryTextField.text = @"Выпечка";
            break;
        }
        case 1: {
            NSLog(@"Морепродукты");
            self.categoryTextField.text = @"Морепродукты";
            break;
        }
        case 2: {
            NSLog(@"Фрукты");
            self.categoryTextField.text = @"Фрукты";
            break;
        }
        case 3: {
            NSLog(@"Напитки");
            self.categoryTextField.text = @"Напитки";
            break;
        }
        case 4: {
            NSLog(@"Мясо");
            self.categoryTextField.text = @"Мясо";
            break;
        }
        case 5: {
            NSLog(@"Овощи");
            self.categoryTextField.text = @"Овощи";
            break;
        }
        default:
            break;
    }
    
}

// Скрыть клавиатуру по нажатию в любом пустом месте экрана приложения
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UIView* view in self.view.subviews)
        [view resignFirstResponder];
}

#pragma mark - Managed Object Context Method

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

@end

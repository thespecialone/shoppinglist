//
//  HistoryTableViewController.m
//  ShoppingList_0.1
//
//  Created by kravinov on 12/24/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "HistoryTableViewController.h"

@interface HistoryTableViewController ()

@end

@implementation HistoryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"HItem"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemName" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSManagedObject *item = [self.items objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %ld$",[item valueForKey:@"itemName"], [[item valueForKey:@"itemPrice"] integerValue]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *strDate = [dateFormatter stringFromDate:[item valueForKey:@"date"]];
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@", [item valueForKey:@"itemCategory"], strDate];
    
    if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Выпечка"]) {
        cell.imageView.image = [UIImage imageNamed:@"bake32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.82 green:0.41 blue:0.12 alpha:0.25];
        
    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Морепродукты"]) {
        cell.imageView.image = [UIImage imageNamed:@"seafood32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.30 green:0.35 blue:0.40 alpha:0.25];
        
    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Фрукты"]) {
        cell.imageView.image = [UIImage imageNamed:@"fruits32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.89 green:0.15 blue:0.21 alpha:0.25];
        
    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Напитки"]) {
        cell.imageView.image = [UIImage imageNamed:@"beverages32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.18 green:0.55 blue:0.34 alpha:0.25];
        
    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Мясо"]) {
        cell.imageView.image = [UIImage imageNamed:@"meat32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.65 green:0.15 blue:0.04 alpha:0.25];
        
    } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Овощи"]) {
        cell.imageView.image = [UIImage imageNamed:@"vegetables32.png"];
        cell.backgroundColor = [UIColor colorWithRed:0.44 green:0.26 blue:0.08 alpha:0.25];
        
    } else {
        cell.imageView.image = nil;
        cell.backgroundColor = nil;
    }
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    if (editingStyle == UITableViewCellEditingStyleDelete){
        [context deleteObject:[self.items objectAtIndex:indexPath.row]];
        
        NSError *error = nil;
        if(![context save:&error]){
            NSLog(@"Failed to remove! %@ %@", error, [error localizedDescription]);
            return;
        }
        
        [self.items removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Managed Object Context Method

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

- (IBAction)ll90FilterButton:(id)sender {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"HItem"];
    if (self.ll90IsPressed == NO) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.ll90IsPressed = YES;
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.ll90IsPressed = NO;
    }
}

- (IBAction)dollarFilterButton:(id)sender {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"HItem"];
    if (self.dollarIsPressed == NO) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemPrice" ascending:YES];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.dollarIsPressed = YES;
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemPrice" ascending:NO];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.dollarIsPressed = NO;
    }
}

- (IBAction)abcFilterButton:(id)sender {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"HItem"];
    if (self.abcIsPressed == NO) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemName" ascending:YES];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.abcIsPressed = YES;
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemName" ascending:NO];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.abcIsPressed = NO;
    }
}

- (IBAction)categoryFilterButton:(id)sender {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"HItem"];
    if (self.cgtIsPressed == NO) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemCategory" ascending:YES];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.cgtIsPressed = YES;
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemCategory" ascending:NO];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        self.cgtIsPressed = NO;
    }
}

#pragma mark - SearchBar Method

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"HItem"];
    
    if (searchText.length != 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemName contains[c]%@", searchText];
        [fetchRequest setPredicate:predicate];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
    }
    else {
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
    }
}


@end

//
//  ListTableViewController.m
//  ShoppingList_0.1
//
//  Created by kravinov on 12/19/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "ListTableViewController.h"

@interface ListTableViewController ()

@end

@implementation ListTableViewController

#pragma mark - View Controller Lifecycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    self.abcIsPressed = YES;
    self.cgtIsPressed = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isListed == YES"];
    [fetchRequest setPredicate:predicate];
    self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *cellIdentifier = @"Cell";
    //static NSString *totalPriceCellIdentifier = @"TotalPriceCell";
    
        NSManagedObject *item = [self.items objectAtIndex:indexPath.row];
        UITableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
//    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", cell.textLabel.text]];
//    
//        [attributeString addAttribute:NSStrikethroughStyleAttributeName
//                                value:@1
//                                range:NSMakeRange(0, [attributeString length])];
//    
//    
//    NSLog(@"%@", [self.items valueForKey:@"isBuyed"]);
//    
//    if ([[item valueForKey:@"isBuyed"]  isEqual:@YES]) {
//        cell.textLabel.attributedText = attributeString;
//    } else {
//        cell.textLabel.attributedText = nil;
//    }
    
    
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [item valueForKey:@"itemName"]];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld$", [[item valueForKey:@"itemPrice"] integerValue]];
    
    
    
        //Заменить на switch!!!
        if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Выпечка"]) {
            cell.imageView.image = [UIImage imageNamed:@"bake32.png"];
            cell.backgroundColor = [UIColor colorWithRed:0.82 green:0.41 blue:0.12 alpha:0.25];
            
        } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Морепродукты"]) {
            cell.imageView.image = [UIImage imageNamed:@"seafood32.png"];
            cell.backgroundColor = [UIColor colorWithRed:0.30 green:0.35 blue:0.40 alpha:0.25];
            
        } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Фрукты"]) {
            cell.imageView.image = [UIImage imageNamed:@"fruits32.png"];
            cell.backgroundColor = [UIColor colorWithRed:0.89 green:0.15 blue:0.21 alpha:0.25];
            
        } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Напитки"]) {
            cell.imageView.image = [UIImage imageNamed:@"beverages32.png"];
            cell.backgroundColor = [UIColor colorWithRed:0.18 green:0.55 blue:0.34 alpha:0.25];
            
        } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Мясо"]) {
            cell.imageView.image = [UIImage imageNamed:@"meat32.png"];
            cell.backgroundColor = [UIColor colorWithRed:0.65 green:0.15 blue:0.04 alpha:0.25];
            
        } else if ([[item valueForKey:@"itemCategory"] isEqualToString:@"Овощи"]) {
            cell.imageView.image = [UIImage imageNamed:@"vegetables32.png"];
            cell.backgroundColor = [UIColor colorWithRed:0.44 green:0.26 blue:0.08 alpha:0.25];
            
        } else {
            cell.imageView.image = nil;
            cell.backgroundColor = nil;
        }
        
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.detailTextLabel.backgroundColor = [UIColor clearColor];

        return cell;
}
//    else {
//        UITableViewCell *totalPriceCell= [tableView dequeueReusableCellWithIdentifier:totalPriceCellIdentifier forIndexPath:indexPath];
//        totalPriceCell.textLabel.text = @"Total Price";
//        totalPriceCell.editing = YES;
//        return totalPriceCell;
//    }


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row != self.items.count;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *item = [self.items objectAtIndex:indexPath.row];
    [item setValue:@NO forKey:@"isListed"];
    [item setValue:@0 forKey:@"itemPrice"];
    [item setValue:@NO forKey:@"isBuyed"];
    
    NSError *error = nil;
    
    NSManagedObjectContext *context = [self managedObjectContext];
    if(![context save:&error]){
        NSLog(@"Failed to remove! %@ %@", error, [error localizedDescription]);
        return;
    }

    [self.items removeObjectAtIndex:indexPath.row];
    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *item = [self.items objectAtIndex:self.tableView.indexPathForSelectedRow.row];
    if (indexPath.row != self.items.count) {
        
        if ([[item valueForKey:@"isBuyed"]  isEqual: @NO]) {
            UIAlertView *priceAlert = [[UIAlertView alloc] initWithTitle:@"Стоимость товара"
                                                                 message:@"Пожалуйста, укажите стоимость товара:"
                                                                delegate:self
                                                       cancelButtonTitle:@"Cancel"
                                                       otherButtonTitles:@"OK", nil];
            [priceAlert setAlertViewStyle:UIAlertViewStylePlainTextInput];
            UITextField *priceField = [priceAlert textFieldAtIndex:0];
            priceField.keyboardType = UIKeyboardTypeNumberPad;
            //Не появляется клавиатура с цифрами!
            //priceField.keyboardType = UIKeyboardTypeNumberPad;
            [priceAlert show];
        } else {
            [item setValue:@NO forKey:@"isBuyed"];
            [item setValue:@0 forKey:@"itemPrice"];
            
            NSError *error = nil;
            
            NSManagedObjectContext *context = [self managedObjectContext];
            if(![context save:&error]){
                NSLog(@"Failed to remove! %@ %@", error, [error localizedDescription]);
                return;
            }
            
            [self.tableView reloadData];
        }
    }
}

#pragma mark - AlertView Delegate Method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            NSLog(@"Cancel");
            break;
        case 1: {
            NSLog(@"OK");
            
            NSError *error = nil;
            
            NSManagedObjectContext *context2 = [self managedObjectContext];
            

            
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
            
            NSManagedObject *item = [self.items objectAtIndex:self.tableView.indexPathForSelectedRow.row];
            
            NSManagedObject *newItem = [NSEntityDescription insertNewObjectForEntityForName:@"HItem" inManagedObjectContext:context2];
            [newItem setValue:[item valueForKey:@"itemName"] forKey:@"itemName"];
            [newItem setValue:[item valueForKey:@"itemCategory"] forKey:@"itemCategory"];
            [newItem setValue:[formatter numberFromString:[[alertView textFieldAtIndex:0] text]] forKey:@"itemPrice"];
            [newItem setValue:[NSDate date] forKey:@"date"];
            
                //[item setValue:[formatter numberFromString:[[alertView textFieldAtIndex:0] text]] forKey:@"itemPrice"];

                [item setValue:@NO forKey:@"isListed"];
                [item setValue:@0 forKey:@"itemPrice"];
                [item setValue:@NO forKey:@"isBuyed"];
                
            if(![context2 save:&error]){
                NSLog(@"Failed to remove! %@ %@", error, [error localizedDescription]);
                return;
            }
            
                NSManagedObjectContext *context = [self managedObjectContext];
                if(![context save:&error]){
                    NSLog(@"Failed to remove! %@ %@", error, [error localizedDescription]);
                    return;
                }
            
            
                
                [self.items removeObjectAtIndex:self.tableView.indexPathForSelectedRow.row];
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:self.tableView.indexPathForSelectedRow] withRowAnimation:UITableViewRowAnimationFade];
                
                break;
            }
            
        default:
            break;
    }
    [self.tableView reloadData];
}

#pragma mark - SearchBar Method

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    
    if (searchText.length != 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isListed == YES && itemName contains[c]%@", searchText];
        [fetchRequest setPredicate:predicate];
    }
    else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isListed == YES"];
        [fetchRequest setPredicate:predicate];
    }
    self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [self.tableView reloadData];
}

#pragma mark - Filter Method's

- (IBAction)abcFilterButton:(id)sender {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isListed == YES"];
    [fetchRequest setPredicate:predicate];
    if (self.abcIsPressed == NO) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemName" ascending:YES];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.abcIsPressed = YES;
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemName" ascending:NO];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.abcIsPressed = NO;
    }
    self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [self.tableView reloadData];
}

- (IBAction)categoryFilterButton:(id)sender {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isListed == YES"];
    [fetchRequest setPredicate:predicate];
    if (self.cgtIsPressed == NO) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemCategory" ascending:YES];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        self.cgtIsPressed = YES;
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemCategory" ascending:NO];
        fetchRequest.sortDescriptors = @[sortDescriptor];
        self.items = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        self.cgtIsPressed = NO;
    }
    [self.tableView reloadData];
}

#pragma mark - Managed Object Context Method

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

@end

//
//  CatalogTableViewController.h
//  ShoppingList_0.1
//
//  Created by kravinov on 12/17/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SWTableViewCell.h"

@interface CatalogTableViewController: UITableViewController <SWTableViewCellDelegate, UISearchBarDelegate>

@property (strong, nonatomic) NSMutableArray *items;
@property BOOL abcIsPressed;
@property BOOL cgtIsPressed;
- (IBAction)abcFilterButton:(id)sender;
- (IBAction)categoryFilterButton:(id)sender;

@end

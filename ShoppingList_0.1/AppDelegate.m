//
//  AppDelegate.m
//  ShoppingList_0.1
//
//  Created by kravinov on 12/17/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.isEmpty = [defaults objectForKey:@"isEmpty"];
    
    if (self.isEmpty == NULL) {
        NSLog(@"Пусто, можно грузить %@",self.isEmpty);
        self.isEmpty = @"1";
        
        NSManagedObjectContext *context = [self managedObjectContext];
        
        NSManagedObject *newItem1 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem1 setValue:@"Арбуз" forKey:@"itemName"];
        [newItem1 setValue:@"Фрукты" forKey:@"itemCategory"];
        
        NSManagedObject *newItem2 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem2 setValue:@"Киви" forKey:@"itemName"];
        [newItem2 setValue:@"Фрукты" forKey:@"itemCategory"];
        
        NSManagedObject *newItem3 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem3 setValue:@"Гранат" forKey:@"itemName"];
        [newItem3 setValue:@"Фрукты" forKey:@"itemCategory"];
        
        NSManagedObject *newItem4 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem4 setValue:@"Тунец" forKey:@"itemName"];
        [newItem4 setValue:@"Морепродукты" forKey:@"itemCategory"];
        
        NSManagedObject *newItem5 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem5 setValue:@"Лобстер" forKey:@"itemName"];
        [newItem5 setValue:@"Морепродукты" forKey:@"itemCategory"];
        
        NSManagedObject *newItem6 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem6 setValue:@"Меч-рыба" forKey:@"itemName"];
        [newItem6 setValue:@"Морепродукты" forKey:@"itemCategory"];
        
        NSManagedObject *newItem7 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem7 setValue:@"Пресная вода" forKey:@"itemName"];
        [newItem7 setValue:@"Напитки" forKey:@"itemCategory"];
        
        NSManagedObject *newItem8 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem8 setValue:@"Яблочный сок" forKey:@"itemName"];
        [newItem8 setValue:@"Напитки" forKey:@"itemCategory"];
        
        NSManagedObject *newItem9 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem9 setValue:@"Кора-рора" forKey:@"itemName"];
        [newItem9 setValue:@"Напитки" forKey:@"itemCategory"];
        
        NSManagedObject *newItem10 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem10 setValue:@"Тостерный хлеб" forKey:@"itemName"];
        [newItem10 setValue:@"Выпечка" forKey:@"itemCategory"];
        
        NSManagedObject *newItem11 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem11 setValue:@"Тульский пряник" forKey:@"itemName"];
        [newItem11 setValue:@"Выпечка" forKey:@"itemCategory"];
        
        NSManagedObject *newItem12 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem12 setValue:@"Батон" forKey:@"itemName"];
        [newItem12 setValue:@"Выпечка" forKey:@"itemCategory"];
        
        NSManagedObject *newItem13 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem13 setValue:@"Медвежий окорок" forKey:@"itemName"];
        [newItem13 setValue:@"Мясо" forKey:@"itemCategory"];
        
        NSManagedObject *newItem14 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem14 setValue:@"Тибон стейк" forKey:@"itemName"];
        [newItem14 setValue:@"Мясо" forKey:@"itemCategory"];
        
        NSManagedObject *newItem15 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem15 setValue:@"Рибай стейк" forKey:@"itemName"];
        [newItem15 setValue:@"Мясо" forKey:@"itemCategory"];
        
        NSManagedObject *newItem16 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem16 setValue:@"Томат" forKey:@"itemName"];
        [newItem16 setValue:@"Овощи" forKey:@"itemCategory"];
        
        NSManagedObject *newItem17 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem17 setValue:@"Спаржа" forKey:@"itemName"];
        [newItem17 setValue:@"Овощи" forKey:@"itemCategory"];
        
        NSManagedObject *newItem18 = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        [newItem18 setValue:@"Картофель" forKey:@"itemName"];
        [newItem18 setValue:@"Овощи" forKey:@"itemCategory"];
        
        NSError *error = nil;
        if(![context save:&error]){
            NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
        }
        else {
            NSLog(@"Saved successfully!");
        }
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:self.isEmpty forKey:@"isEmpty"];

        [defaults synchronize];
        return YES;
    } else {
        NSLog(@"Загруженно!!!!!");
        return YES;
    }

}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ShoppingList_0_1" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
//    NSString *storePath = [[[self applicationDocumentsDirectory] path] stringByAppendingPathComponent: @"ShoppingList_0_1.sqlite"];
//    NSURL *storeUrl = [NSURL fileURLWithPath:storePath];
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    if (![fileManager fileExistsAtPath:storePath]) {NSString *defaultStorePath = [[NSBundle mainBundle]
//                                                                                  pathForResource:@"ShoppingList_0_1" ofType:@"sqlite"];
//        if (defaultStorePath) {[fileManager copyItemAtPath:defaultStorePath
//                                                    toPath:storePath error:NULL];
//        }}
    
    
    
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ShoppingList.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {

        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
